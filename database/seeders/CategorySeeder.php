<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'title' => 'Эконом',
                'landing_price' => 20,
                'price_for_km' => 12,
                'price_for_waiting_minute' => 3,
            ],
            [
                'title' => 'Эконом+',
                'landing_price' => 25,
                'price_for_km' => 13,
                'price_for_waiting_minute' => 3,
            ],
            [
                'title' => 'Бизнес',
                'landing_price' => 30,
                'price_for_km' => 15,
                'price_for_waiting_minute' => 5,
            ],
            [
                'title' => 'Грузовое',
                'landing_price' => 80,
                'price_for_km' => 20,
                'price_for_waiting_minute' => 5,
            ],
        ];

        foreach ($items as $item) {
            \DB::table('categories')->insert(array_merge($item, [
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]));
        }
    }
}
