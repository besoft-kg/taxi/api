<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'phone_number' => '996777171171',
                'full_name' => 'Bekbolot Tazhibaev',
            ],
            [
                'phone_number' => '996556551999',
                'full_name' => 'Adilet Makhamatov',
            ],
        ];

        foreach ($items as $item) {
            \DB::table('admins')->insert(array_merge($item, [
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]));
        }
    }
}
