<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('published_by');
            $table->timestamp('published_at')->nullable();
            $table->string('version_name', 15);
            $table->integer('version_code');
            $table->enum('platform', ['android', 'web', 'ios']);
            $table->json('data')->nullable();
            $table->string('link', 255)->nullable();

            $table->foreign('published_by')->references('id')->on('admins');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('versions');
    }
}
