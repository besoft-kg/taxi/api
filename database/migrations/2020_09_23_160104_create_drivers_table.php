<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();

            $table->string('phone_number', 55)->unique();
            $table->string('full_name', 255)->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->json('contacts')->nullable();
            $table->unsignedBigInteger('picture_id')->nullable();
            $table->integer('balance')->default(0);

            $table->json('categories')->nullable();
            $table->unsignedBigInteger('confirmed_by_id')->nullable();
            $table->datetime('confirmed_at')->nullable();

            $table->unsignedBigInteger('vehicle_picture_id')->nullable();
            $table->string('state_reg_plate')->nullable();
            $table->unsignedBigInteger('datasheet_picture_id')->nullable();

            $table->unsignedBigInteger('drivers_license_picture_id')->nullable();
            $table->date('drivers_license_expiration_date')->nullable();

            $table->timestamp('last_action')->useCurrent();
            $table->boolean('busy')->default(false);

            $table->float('location_lat', 10, 6)->nullable();
            $table->float('location_lng', 10, 6)->nullable();
            $table->timestamp('location_last_refresh')->nullable();

            $table->foreign('picture_id')->references('id')->on('images');
            $table->foreign('vehicle_picture_id')->references('id')->on('images');
            $table->foreign('drivers_license_picture_id')->references('id')->on('images');
            $table->foreign('datasheet_picture_id')->references('id')->on('images');
            $table->foreign('confirmed_by_id')->references('id')->on('admins');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
