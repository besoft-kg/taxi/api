<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('publisher_id')->nullable();
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('executor_id')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->string('phone_number')->nullable();
            $table->enum('status', [
                'pending',
                'fake_call',
                'fake_call_checking',
                'cancelled_by_client',
                'cancelled_by_admin',
                'going_to_client',
                'in_place',
                'going_with_client',
                'completed',
            ])->default('pending');
            $table->text('note')->nullable();
            $table->json('history')->nullable();
            $table->json('global_history')->nullable();
            $table->json('payload')->nullable();

            $table->string('address', 255)->nullable();
            $table->float('address_lat', 10, 6)->nullable();
            $table->float('address_lng', 10, 6)->nullable();

            $table->foreign('publisher_id')->references('id')->on('admins');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('executor_id')->references('id')->on('drivers');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
