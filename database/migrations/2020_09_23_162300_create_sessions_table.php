<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id');
            $table->enum('user_type', ['admin', 'driver', 'client']);
            $table->string('key', 255);
            $table->text('user_agent')->nullable();
            $table->text('fcm_token')->nullable();
            $table->timestamp('expired_at');
            $table->enum('platform', ['android', 'ios', 'web']);
            $table->integer('version_code');
            $table->timestamp('last_action')->useCurrent();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions');
    }
}
