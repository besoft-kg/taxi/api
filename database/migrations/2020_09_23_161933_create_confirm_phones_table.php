<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirm_phones', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('sms_id');
            $table->bigInteger('user_id')->nullable();
            $table->enum('user_type', ['admin', 'driver', 'client']);
            $table->smallInteger('attempts')->default(10);
            $table->string('phone_number', 55);
            $table->string('encrypted_code', 255);
            $table->boolean('confirmed')->default(false);

            $table->foreign('sms_id')->references('id')->on('sms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirm_phones');
    }
}
