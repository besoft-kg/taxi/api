<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlarmsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('alarms', function (Blueprint $table) {
			$table->id();

			$table->unsignedBigInteger('driver_id');
			$table->float('address_lat', 10, 6)->nullable();
			$table->float('address_lng', 10, 6)->nullable();

			$table->foreign('driver_id')->references('id')->on('drivers');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('alarms');
	}
}
