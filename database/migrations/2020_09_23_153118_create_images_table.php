<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('user_id');
            $table->enum('user_type', ['admin', 'driver', 'client']);
            $table->string('name', 255);
            $table->string('hash', 255);
            $table->json('path');
            $table->json('size');
            $table->json('dimensions');
            $table->string('extension', 35);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
