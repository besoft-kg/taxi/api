<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->id();

            $table->string('external_id', 255)->nullable();
            $table->string('phone_number', 55);
            $table->string('country', 255)->nullable();
            $table->string('region', 255)->nullable();
            $table->string('operator', 255)->nullable();
            $table->text('content');
            $table->integer('parts');
            $table->float('amount');
            $table->float('price');
            $table->string('status', 35);
            $table->json('response')->nullable();
            $table->json('delivery_report')->nullable();
            $table->string('used_for', 35)->nullable();
            $table->json('payload')->nullable();
            $table->string('service', 55);
            $table->boolean('test');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
