<?php

namespace App\Utils;

use App\Models\Session;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Crypt;
use GuzzleHttp;

class Generator
{
    static function jwt($user, $sub, $exp)
    {
        // $payload = [
        //     'iss' => "besoft taxi",
        //     'sub' => ['user' => $user->id, 'key' => $sub],
        //     'iat' => Carbon::now()->timestamp,
        //     'exp' => $exp
        // ];

        // return JWT::encode($payload, env('JWT_SECRET'));

        return Crypt::encrypt($sub);
    }

    static function verificationCode()
    {
        if (env('APP_DEBUG')) return 1234;
        return rand(1000, 9999);
    }

    static function randomString($length = 32)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function sessionKey()
    {
        $key = Generator::randomString(64);

        if (Session::where('key', $key)->exists()) {
            return Generator::sessionKey();
        }

        return $key;
    }

    static function geocoder($lat, $lng, $lang)
    {

        $client = new GuzzleHttp\Client();

        $response = $client->request('GET', 'https://geocode-maps.yandex.ru/1.x', [
            'query' => [
                'geocode' => "$lat,$lng",
                'apikey' => '0373d54b-b555-4e36-b355-e988091ded3d',
                'sco' => 'latlong',
                'format' => 'json',
                'kind' => 'house',
                'lang' => $lang . '_KG',
            ]
        ]);

        $body = json_decode((string) $response->getBody(), true);
        $object = $body['response']['GeoObjectCollection']['featureMember'][0]['GeoObject'];
        $point = explode(' ', $object['Point']['pos']);

        return [
            'address' => $object['name'],
            'lat' => $point[1],
            'lng' => $point[0],
        ];
    }
}
