<?php

namespace App\Services;

use App\Models\Sms;
use \Exception;

class SmsService
{
    private $phone;
    private $content;
    private $used_for;
    private $payload;
    private $test;

    private $external_id;

    public function __construct($phone, $content, $used_for = '', $payload = [], $test = false)
    {
        $this->phone = $phone;
        $this->content = 'Taxi: ' . $content;
        $this->used_for = $used_for;
        $this->payload = $payload;
        $this->test = $test;
    }

    public function send()
    {
        $url = "http://smspro.nikita.kg/api/message";
        $uagent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";
        $this->external_id = "Besoft_Taxi_SMS_" . time();

        $xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
            "<message>" .
            "<login>" . env('NIKITA_LOGIN') . "</login>" .
            "<pwd>" . env('NIKITA_PASSWORD') . "</pwd>" .
            "<id>" . $this->external_id . "</id>" .
            "<sender>" . "Besoft" . "</sender>" .
            "<text>" . $this->content . "</text>" .
            // "<time>20101118214600</time>".   // Можно указать время отправки этого сообщения
            "<phones>" .
            "<phone>" . $this->phone . "</phone>" .
            // "<phone>996772947637</phone>".   // Можно указать дополнительные номера телефонов
            "</phones>";

        if (env('APP_DEBUG') || $this->test) $xml .= "<test>1</test>";

        $xml .= "</message>";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_USERAGENT, $uagent);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_COOKIEJAR, "c://coo.txt");
        curl_setopt($ch, CURLOPT_COOKIEFILE, "c://coo.txt");

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = json_decode(json_encode(simplexml_load_string($content)), true);

        if ($err == 0 && $errmsg == "" && in_array($header['content']['status'], ['0', '11'])) {
            return $this->intertToDb($header['content']);
        } elseif ($err != 0) {
            throw new Exception($errmsg);
        } else throw new Exception();
    }

    public function intertToDb($response)
    {
        $sms = new Sms();

        $sms->external_id = $this->external_id;
        $sms->phone_number = $this->phone;
        $sms->content = $this->content;
        $sms->parts = $response['smscnt'];
        $sms->amount = 1.19;
        $sms->price = 1.19 * $sms->parts;
        $sms->status = 'sent_to_service';
        $sms->response = $response;
        $sms->used_for = $this->used_for;
        $sms->payload = $this->payload;
        $sms->service = 'smspro_nikita';
        $sms->test = $this->test;

        $sms->save();

        return $sms;
    }
}
