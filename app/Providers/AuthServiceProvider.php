<?php

namespace App\Providers;

use App\Exceptions\ResponseException;
use App\Models\Driver;
use App\Models\Session;
use App\Models\Admin;
use App\Models\Client;
//use Firebase\JWT\ExpiredException;
//use Firebase\JWT\JWT;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Facades\Crypt;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Boot the authentication services for the application.
   *
   * @return void
   */
  public function boot()
  {
    // Here you may define how you wish users to be authenticated for your Lumen
    // application. The callback which receives the incoming request instance
    // should return either a User instance or null. You're free to obtain
    // the User instance via an API token or any other method necessary.

    $this->app['auth']->viaRequest('api', function ($request) {
      $header = $request->header('Authorization', '');
      $token = null;

      if (Str::startsWith($header, 'Bearer ')) {
        $token = Str::substr($header, 7);
      }

      if ($token) {
        try {
          //$credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
          $credentials = Crypt::decrypt($token);
          //} catch (ExpiredException $e) {
          //throw new ResponseException('', 'token_is_expired');
        } catch (Exception $e) {
          throw new ResponseException('', 'token_is_invalid');
        }

        //$session = Session::where('key', $credentials->sub->key)->where('user_id', $credentials->sub->user)->first();
        $session = Session::where('key', $credentials)->first();

        if (!$session) {
          throw new ResponseException('', 'token_is_invalid');
        } else {
          //Session::where('user_id', $credentials->sub->user)->where('platform', $session->platform)->where('id', '!=', $session->id)->delete();
          Session::where('user_id', $session->user_id)->where('key', $session->key)->where('platform', $session->platform)->where('id', '!=', $session->id)->delete();
        }

        $user = null;

        switch ($session->user_type) {
          case 'admin':
            $user = Admin::where('id', $session->user_id)->first();
            break;

          case 'driver':
            $user = Driver::where('id', $session->user_id)->first();
            break;

          case 'client':
            $user = Client::where('id', $session->user_id)->first();
            break;

          default:
            break;
        }

        return $user;
      } else {
        return null;
      }
    });
  }
}
