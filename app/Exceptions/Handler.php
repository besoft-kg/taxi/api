<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        ResponseException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param IlluminateHttpRequest $request
     * @param Throwable $exception
     * @return IlluminateHttpResponse|IlluminateHttpJsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        //parent::render($request, $exception);
        if ($exception instanceof ValidationException) {
            return response([$exception->getResponse()->original, 'invalid_params', -1]);
        } else if ($exception instanceof ResponseException) {
            return response([['message' => $exception->getMessage()], $exception->getCode(), -1]);
        } else if ($exception instanceof MethodNotAllowedHttpException) {
            return response([null, 'method_not_allowed', -1], $exception->getStatusCode());
        } else if ($exception instanceof AuthorizationException) {
            return response([['message' => $exception->getMessage()], 'access_denied', -1], 403);
        } else {
            return response([['message' => $exception->getMessage()], 'unknown_error', -1]);
        }
    }
}
