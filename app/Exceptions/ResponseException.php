<?php

namespace App\Exceptions;

use Exception;

class ResponseException extends Exception
{
    public function __construct($message = "", $status = 'unknown_error')
    {
        $this->message = $message;
        $this->code = $status;
    }
}
