<?php

namespace App\Models;

class Sms extends BaseModel
{
    protected $table = 'sms';

    protected $fillable = [
        'external_id', 'phone_number',
        'country', 'region',
        'operator', 'content',
        'parts', 'amount',
        'price', 'status',
        'response', 'delivery_report',
        'used_for', 'payload',
        'service', 'test',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'payload' => 'array',
        'response' => 'array',
        'delivery_report' => 'array',
        'test' => 'boolean',
    ];
}
