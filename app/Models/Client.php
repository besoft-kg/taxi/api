<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;

class Client extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
  use Authenticatable, Authorizable;

  protected $table = 'clients';

  protected $fillable = [
    'phone_number',
    'full_name',
    'contacts',
    'last_action',
  ];

  protected $casts = [
    'contacts' => 'array',
    'last_action' => 'datetime',
  ];

  protected $guarded = [
    'id',
    'updated_at',
    'created_at'
  ];

  public function getTypeAttribute()
  {
    return 'client';
  }

  public function getIsDriverAttribute()
  {
    return false;
  }

  public function getIsAdminAttribute()
  {
    return false;
  }

  public function getIsClientAttribute()
  {
    return true;
  }
}
