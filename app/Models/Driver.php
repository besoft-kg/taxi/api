<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;

class Driver extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'drivers';

    protected $fillable = [
        'phone_number',
        'full_name',
        'gender',
        'contacts',
        'picture_id',
        //'balance',
        //'categories',
        //'confirmed_by_id',
        //'confirmed_at',
        'vehicle_picture_id',
        //'state_reg_plate',
        'datasheet_picture_id',
        'drivers_license_picture_id',
        'drivers_license_expiration_date',
        'last_action',
        'busy',
        'location_lat',
        'location_lng',
        'location_last_refresh',
    ];

    protected $casts = [
        'contacts' => 'array',
        'busy' => 'boolean',
        'categories' => 'array',
        'confirmed_at' => 'datetime',
        'drivers_license_expiration_date' => 'date',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at'
    ];

    public function getIsDriverAttribute()
    {
        return true;
    }

    public function getIsAdminAttribute()
    {
        return false;
    }

    public function getIsClientAttribute()
    {
        return false;
    }

    public function getTypeAttribute()
    {
        return 'driver';
    }

    public function picture()
    {
        return $this->hasOne('App\Models\Image', 'id', 'picture_id');
    }

    public function datasheet_picture()
    {
        return $this->hasOne('App\Models\Image', 'id', 'datasheet_picture_id');
    }

    public function drivers_license_picture()
    {
        return $this->hasOne('App\Models\Image', 'id', 'drivers_license_picture_id');
    }

    public function vehicle_picture()
    {
        return $this->hasOne('App\Models\Image', 'id', 'vehicle_picture_id');
    }
}
