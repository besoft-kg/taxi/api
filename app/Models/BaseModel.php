<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public function scopeDynamic($query)
    {
        return $query;
    }

    public static function exists($id) {
        return self::where('id', $id)->exists();
    }
}
