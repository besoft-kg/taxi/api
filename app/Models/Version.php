<?php

namespace App\Models;

class Version extends BaseModel
{
    protected $table = 'versions';
    protected $with = [];
    protected $dates = ['created_at', 'updated_at', 'published_at'];
    protected $appends = [];
    protected $casts = ['data' => 'array'];

    protected $fillable = [
        'published_at',
        'version_name',
        'version_code',
        'platform',
        'data',
        'link'
    ];

    protected $guarded = [
        'id',
        'published_by',
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
    ];
}
