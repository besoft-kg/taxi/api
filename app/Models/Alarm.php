<?php

namespace App\Models;

class Alarm extends BaseModel
{
  protected $table = 'alarms';
  protected $dates = ['created_at', 'updated_at', 'ended_at'];

  protected $fillable = [
    'driver_id',
    'ended_at',
    'address_lat',
    'address_lng',
  ];

  protected $guarded = [
    'updated_at',
    'created_at',
  ];

  public function driver()
  {
    return $this->hasOne('App\Models\Driver', 'id', 'driver_id');
  }
}
