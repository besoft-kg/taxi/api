<?php

namespace App\Models;

class Taximeter extends BaseModel
{
    protected $table = 'taximeters';
    protected $dates = ['created_at', 'updated_at', 'started_at', 'ended_at'];

    protected $fillable = [
        'order_id',
        'driver_id',
        'started_at',
        'ended_at',
        'amount_to_pay',
        'duration_in_minutes',
        'waiting_time_in_minutes',
        'distance_in_m',
        'amount_info',
        'locations',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
    ];

    protected $casts = [
        'amount_info' => 'array',
        'locations' => 'array',
    ];
}
