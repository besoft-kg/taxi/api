<?php

namespace App\Models;

class Category extends BaseModel
{
    protected $table = 'categories';

    protected $fillable = [
        'title',
        'parent_id',
        'landing_price',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at'
    ];
}
