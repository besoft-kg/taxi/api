<?php

namespace App\Models;

class Order extends BaseModel
{
    protected $table = 'orders';

    protected $fillable = [
        'client_id',
        'category_id',
        'phone_number',
        'note',
        'payload',
        'address',
        'address_lat',
        'address_lng',
    ];

    protected $hidden = ['global_history'];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at'
    ];

    protected $casts = [
        'history' => 'array',
        'global_history' => 'array',
        'payload' => 'array',
    ];

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function executor()
    {
        return $this->hasOne('App\Models\Driver', 'id', 'executor_id');
    }

    public function taximeter()
    {
        return $this->hasOne('App\Models\Taximeter', 'order_id', 'id');
    }

    //    public function cancelled()
    //    {
    //        return in_array($this->status, [
    //            'cancelled_by_admin',
    //            'cancelled_by_client',
    //        ]);
    //    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [
            'going_to_client',
            'in_place',
            'going_with_client',
        ]);
    }
}
