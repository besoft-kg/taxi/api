<?php

namespace App\Models;

class Session extends BaseModel
{
    protected $table = 'sessions';
    protected $dates = ['created_at', 'updated_at', 'expired_at', 'last_action'];

    protected $fillable = [
        'user_agent',
        'fcm_token',
        'platform',
        'version_code',
    ];

    protected $guarded = [
        'id',
        'user_id',
        'key',
        'expired_at',
        'updated_at',
        'created_at',
        'last_action',
    ];

    protected $hidden = [
        'key',
    ];
}
