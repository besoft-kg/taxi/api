<?php

namespace App\Events\Driver;

use App\Models\Order;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Auth;

class OnUpdateOrderEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $item;

    public function __construct($item)
    {
        if ($item->client_id && !$item->client && Auth::user()->id === $item->executor_id) {
            $item = Order::with('client')->find($item->id);
        }

        $this->item = $item;
    }

    public function broadcastOn()
    {
        return ['private-driver.orders'];
    }

    public function broadcastAs()
    {
        return 'on-update';
    }
}
