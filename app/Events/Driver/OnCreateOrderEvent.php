<?php

namespace App\Events\Driver;

use App\Models\Order;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OnCreateOrderEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $item;

    public function __construct($item)
    {
        $this->item = $item;
    }

    public function broadcastOn()
    {
        return ['private-driver.orders'];
    }

    public function broadcastAs()
    {
        return 'on-create';
    }
}
