<?php

namespace App\Events\Driver;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OnUpdateUserEvent implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $item;

    public function __construct($item)
    {
        $this->item = $item;
    }

    public function broadcastOn()
    {
        return ['private-driver.' . $this->item->id];
    }

    public function broadcastAs()
    {
        return 'on-update';
    }
}
