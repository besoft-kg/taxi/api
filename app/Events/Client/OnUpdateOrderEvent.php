<?php

namespace App\Events\Client;

use App\Models\Order;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OnUpdateOrderEvent implements ShouldBroadcast
{
  use InteractsWithSockets, SerializesModels;

  public $item;

  public function __construct($item)
  {
    if ($item->client_id) {
      $item = Order::with('executor')->find($item->id);
    }

    $this->item = $item;
  }

  public function broadcastOn()
  {
    return ['private-client.' . $this->item->client_id . '.order.' . $this->item->id];
  }

  public function broadcastAs()
  {
    return 'on-update';
  }
}
