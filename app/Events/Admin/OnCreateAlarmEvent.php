<?php

namespace App\Events\Admin;

use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OnCreateAlarmEvent implements ShouldBroadcast
{
  use InteractsWithSockets, SerializesModels;

  public $item;

  public function __construct($item)
  {
    $this->item = $item;
  }

  public function broadcastOn()
  {
    return ['private-admin.alarm'];
  }

  public function broadcastAs()
  {
    return 'on-create';
  }
}
