<?php

namespace App\Http\Middleware;

use Closure;

class AppMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $payload = $response->original[0];
        $status = isset($response->original[1]) ? $response->original[1] : 'success';
        $result = isset($response->original[2]) ? $response->original[2] : 0;

        $status_code = $response->getStatusCode();

        if ($status_code === 404) {
            return response([
                'payload' => $response->original,
                'status' => 'page_not_found',
                'result' => -1,
            ], $status_code);
        }

        return response([
            'payload' => $payload,
            'status' => $status,
            'result' => $result,
        ], $status_code);
    }
}
