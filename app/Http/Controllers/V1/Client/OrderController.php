<?php

namespace App\Http\Controllers\V1\Client;

use App\Events\Admin\OnCreateOrderEvent as OnCreateOrderAdminEvent;
use App\Events\Driver\OnCreateOrderEvent as OnCreateOrderDriverEvent;
use App\Events\Driver\OnUpdateOrderEvent as OnUpdateOrderDriverEvent;
use App\Events\Admin\OnUpdateOrderEvent as OnUpdateOrderAdminEvent;
use App\Exceptions\ResponseException;
use App\Models\Order;
use App\Models\Category;
use App\Http\Controllers\V1\Controller;
use Carbon\Carbon;

class OrderController extends Controller
{
  public function postIndex()
  {
    /*
         * Response:
         *      category_not_found
         *      client_has_order
         */

    $this->_validate([
      'category_id' => 'bail|required|integer',
      'address' => 'bail|string|max:250',
      'address_lat' => 'bail|numeric',
      'address_lng' => 'bail|numeric',
    ]);

    $category_id = $this->request->get('category_id');
    $address = $this->request->get('address');
    $address_lng = $this->request->get('address_lng');
    $address_lat = $this->request->get('address_lat');

    if (Order::where('client_id', $this->user->id)
      ->whereIn('status', [
        'pending',
        'fake_call_checking',
        'going_to_client',
        'in_place',
        'going_with_client',
      ])->count() > 0
    ) return [null, 'client_has_order'];

    if (!Category::where('id', $category_id)->exists()) return [null, 'category_not_found'];

    if (
      (!$this->request->has('address') || strlen($address) === 0) &&
      (!$this->request->has('address_lat') ||
        !$this->request->has('address_lng') ||
        strlen($address_lat) === 0 ||
        strlen($address_lng) === 0)
    ) return [null, 'invalid_address'];

    $item = new Order();
    $item->client_id = $this->user->id;
    $item->category_id = $category_id;
    $item->address = $address;
    if ($address_lat) $item->address_lat = $address_lat;
    if ($address_lng) $item->address_lng = $address_lng;

    if (!$item->save()) throw new ResponseException();
    $item = Order::find($item->id);

    event(new OnCreateOrderDriverEvent($item));
    event(new OnCreateOrderAdminEvent($item));

    return [$item];
  }

  public function postCancel()
  {
    /*
         * Response:
         *      -item_not_found
         *      -already_cancelled
         */

    $this->_validate([
      'id' => 'bail|required|integer',
    ]);

    $item = Order::where('id', $this->request->get('id'))->where('client_id', $this->user->id)->first();

    if (!$item) return [null, 'item_not_found'];
    if (in_array($item->status, [
      'cancelled_by_admin', 'cancelled_by_client',
    ])) return [null, 'already_cancelled'];

    $history = [
      'changed_from' => $item->status,
      'changed_to' => 'cancelled_by_client',
      'changed_by' => $this->user->type . '_' . $this->user->id,
      'changed_at' => Carbon::now(),
    ];

    $item->global_history = array_merge([$history], $item->global_history ? $item->global_history : []);
    $item->history = null;
    $item->status = 'cancelled_by_client';
    $item->executor_id = null;

    if (!$item->save()) throw new ResponseException();

    event(new OnUpdateOrderDriverEvent($item));
    event(new OnUpdateOrderAdminEvent($item));

    return [$item];
  }
}
