<?php

namespace App\Http\Controllers\V1\Client;

use App\Http\Controllers\V1\Controller;
use App\Models\Category;
use App\Models\Client;
use App\Models\Order;
use App\Utils\Generator;

class UserController extends Controller
{
  public function getBasic()
  {
    return [
      [
        'order' => Order::with('executor')->where('client_id', $this->user->id)->whereIn('status', [
          'pending',
          'fake_call_checking',
          'going_to_client',
          'in_place',
          'going_with_client',
        ])->first(),
        'user' => Client::where('id', $this->user->id)->first(),
        'categories' => Category::all(),
      ]
    ];
  }

  public function postGeocode()
  {
    /*
         * Response:
         *      category_not_found
         *      client_has_order
         */

    $this->_validate([
      'lat' => 'bail|required|numeric',
      'lng' => 'bail|required|numeric',
      'language' => 'bail|required|string|in:ky,ru,en,uz',
    ]);

    $geocoded = Generator::geocoder($this->request->get('lat'), $this->request->get('lng'), $this->request->get('language'));

    return [$geocoded];
  }
}
