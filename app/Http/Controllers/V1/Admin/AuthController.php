<?php

namespace App\Http\Controllers\V1\Admin;

use App\Models\ConfirmPhone;
use App\Models\Session;
use App\Models\Admin;
use App\Services\SmsService;
use App\Utils\Generator;
use Carbon\Carbon;
use App\Http\Controllers\V1\Controller;

class AuthController extends Controller
{
    public function checkPhone()
    {
        /*
         * Response:
         *      code_was_sent
         *      code_is_sent
         *      admin_not_found
         */

        $this->_validate([
            'phone_number' => 'bail|required|regex:/^[9][9][6][0-9]{9}$/'
        ]);

        $phone_number = $this->request->get('phone_number');

        $user = Admin::where('phone_number', $phone_number)->first();

        if (!$user) return [null, 'admin_not_found'];
        
        if (ConfirmPhone::where('user_id', $user->id)->where('user_type', 'admin')->where('confirmed', false)->where('attempts', '>', 0)->first()) {
            return [null, 'code_was_sent'];
        } else {
            return $this->_dbTransactionAndTryCatch(function () use ($user) {
                $code = $user->phone_number === '996504290100' ? 9345 : Generator::verificationCode();

                $sms = new SmsService(
                    $user->phone_number,
                    'Код подтверждения вашего номера: ' . $code,
                    'sign-in',
                    ['code' => $code],
                    $user->phone_number === '996504290100'
                );

                $sms = $sms->send();

                $confirm_phone = new ConfirmPhone();
                $confirm_phone->sms_id = $sms->id;
                $confirm_phone->user_id = $user->id;
                $confirm_phone->user_type = 'admin';
                $confirm_phone->phone_number = $user->phone_number;
                $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

                $confirm_phone->save();

                return [null, 'code_is_sent'];
            });
        }
    }

    public function getUser()
    {
        return [$this->user];
    }

    public function loginPhone()
    {
        /*
         * Response:
         *      -code_is_incorrect
         *      -code_is_invalid
         *      -admin_not_found
         */

        $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'code' => 'bail|required|integer|digits:4',
            'platform' => 'bail|required|string|in:web,android,ios',
            'version_code' => 'bail|required|integer',
        ]);

        $phone_number = $this->request->get('phone_number');
        $code = $this->request->get('code');

        $user = Admin::where('phone_number', $phone_number)->first();

        if (!$user) return [null, 'admin_not_found'];

        $confirm_phone = ConfirmPhone::where('user_id', $user->id)->where('user_type', 'admin')->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) return [null, 'code_is_invalid'];

        return $this->_dbTransactionAndTryCatch(function () use ($confirm_phone, $code, $user) {
            if ($confirm_phone->encrypted_code === md5($code . env('APP_KEY'))) {
                $confirm_phone->confirmed = true;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $sessionKey = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->user_type = 'admin';
                $session->key = $sessionKey;
                $session->expired_at = $expired_at;
                $session->version_code = $this->request->get('version_code');
                $session->platform = $this->request->get('platform');
                $session->user_agent = $this->request->header('User-Agent');

                $session->save();

                if ($user->isDirty()) $user->save();

                return [[
                    'token' => Generator::jwt($user, $sessionKey, $expired_at->timestamp),
                    'user' => $user,
                ]];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                return [['attempts' => $confirm_phone->attempts], 'code_is_incorrect'];
            }
        });
    }
}
