<?php

namespace App\Http\Controllers\V1\Admin;

use App\Events\Driver\OnUpdateUserEvent as OnUpdateUserDriverEvent;
use App\Exceptions\ResponseException;
use App\Models\Driver;
use App\Http\Controllers\V1\Controller;
use Carbon\Carbon;

class DriverController extends Controller
{
    public function get()
    {
        /*
         * Response:
         *      item_not_found
         */

        $this->_validate([
            'id' => 'bail|integer',
        ]);

        if ($this->request->has('id')) {

            $item = Driver::with([
                'picture',
                'datasheet_picture',
                'drivers_license_picture',
                'vehicle_picture',
            ])->find($this->request->get('id'));
            if (!$item) return [null, 'item_not_found'];
            return [$item];

        } else {

            return [
                [
                    'items' => Driver::limit(30)
                        ->orderBy('created_at', 'desc')
                        ->get(),
                    'not_confirmed_items' => Driver::with([
                        'picture',
                        'datasheet_picture',
                        'drivers_license_picture',
                        'vehicle_picture',
                    ])
                        ->whereNull('confirmed_by_id')
                        ->whereNull('confirmed_at')
                        ->whereNotNull('picture_id')
                        ->whereNotNull('vehicle_picture_id')
                        ->whereNotNull('datasheet_picture_id')
                        ->whereNotNull('drivers_license_picture_id')
                        ->limit(30)
                        ->orderBy('created_at', 'desc')
                        ->get(),
                ]
            ];

        }

    }

    public function postConfirm()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_confirmed
         *      -documents_are_not_uploaded
         */

        $this->_validate([
            'id' => 'bail|required|integer',
            'gender' => 'bail|required|string|in:male,female',
            'full_name' => 'bail|required|string|min:3|max:250',
            'categories' => 'bail|required|array',
            'state_reg_plate' => 'bail|required|string',
            'drivers_license_expiration_date' => 'bail|required|date',
        ]);

        $item = Driver::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];
        if (
            $item->confirmed_at &&
            $item->confirmed_by_id
        ) return [null, 'already_confirmed'];

        $item->confirmed_by_id = $this->user->id;
        $item->confirmed_at = Carbon::now();

        $item->gender = $this->request->get('gender');
        $item->full_name = $this->request->get('full_name');
        $item->categories = $this->request->get('categories');
        $item->state_reg_plate = strtoupper($this->request->get('state_reg_plate'));
        $item->drivers_license_expiration_date = $this->request->get('drivers_license_expiration_date');

        if (!$item->save()) throw new ResponseException();
        $item = Driver::with([
            'picture',
            'datasheet_picture',
            'drivers_license_picture',
            'vehicle_picture',
        ])->find($item->id);

        event(new OnUpdateUserDriverEvent($item));

        return [$item];
    }
}
