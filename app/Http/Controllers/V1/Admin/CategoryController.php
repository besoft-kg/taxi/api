<?php

namespace App\Http\Controllers\V1\Admin;

use App\Models\Category;
use App\Http\Controllers\V1\Controller;
use App\Exceptions\ResponseException;

class CategoryController extends Controller
{
    public function create()
    {
        /*
         * Response:
         *      exists
         *      parent_not_found
         */

        $this->_validate([
            'title' => 'bail|required|string|max:250',
            'parent_id' => 'bail|integer',
        ]);

        $title = $this->request->get('title');
        $parent_id = $this->request->get('parent_id');

        $exists = Category::where('title', $title);
        if ($parent_id) $exists = $exists->where('parent_id', $parent_id);
        else $exists = $exists->whereNull('parent_id');
        if ($exists->exists()) return [null, 'exists'];

        if ($parent_id && !Category::where('id', $parent_id)->exists()) return [null, 'parent_not_found'];

        $item = new Category();
        $item->title = $title;
        $item->parent_id = $parent_id;

        if (!$item->save()) throw new ResponseException();

        return [Category::find($item->id)];
    }

    public function update()
    {
        /*
         * Response:
         *      exists
         *      item_not_found
         *      parent_not_found
         */

        $this->_validate([
            'id' => 'bail|required|integer',
            'title' => 'bail|required|string|max:250',
            'parent_id' => 'bail|integer',
        ]);

        $item = Category::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];

        $title = $this->request->get('title');
        $parent_id = $this->request->get('parent_id');

        if ($parent_id === $item->id) throw new ResponseException();

        $exists = Category::where('id', '!=', $item->id)->where('title', $title);
        if ($parent_id) $exists = $exists->where('parent_id', $parent_id);
        else $exists = $exists->whereNull('parent_id');
        if ($exists->exists()) return [null, 'exists'];

        $parent = Category::find($parent_id);

        if ($parent_id && !$parent) return [null, 'parent_not_found'];
        if ($parent->parent_id === $item->id) throw new ResponseException();

        $item->title = $title;
        $item->parent_id = $parent_id;

        if (!$item->save()) throw new ResponseException();

        return [$item];
    }

    public function delete()
    {
        /*
         * Response:
         *      item_not_found
         *      cant_delete
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Category::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];

        if (Category::where('parent_id', $item->id)->count() > 0) return [null, 'cant_delete'];

        if (!$item->delete()) throw new ResponseException();

        return [null];
    }

    public function get()
    {
        /*
         * Response:
         *      item_not_found
         */

        $this->_validate([
            'id' => 'bail|integer',
        ]);

        if ($this->request->has('id')) {

            $item = Category::find($this->request->get('id'));
            if (!$item) return [null, 'item_not_found'];
            return [$item];

        } else {

            return [Category::all()];

        }
    }
}
