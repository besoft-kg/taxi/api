<?php

namespace App\Http\Controllers\V1\Admin;

use App\Events\Admin\OnCreateOrderEvent as OnCreateOrderAdminEvent;
use App\Events\Driver\OnCreateOrderEvent as OnCreateOrderDriverEvent;
use App\Events\Driver\OnUpdateOrderEvent as OnUpdateOrderDriverEvent;
use App\Events\Client\OnUpdateOrderEvent as OnUpdateOrderClientEvent;
use App\Exceptions\ResponseException;
use App\Models\Order;
use App\Models\Category;
use App\Http\Controllers\V1\Controller;
use Carbon\Carbon;

class OrderController extends Controller
{
    public function postIndex()
    {
        /*
         * Response:
         *      category_not_found
         */

        $this->_validate([
            'category_id' => 'bail|required|integer',
            'phone_number' => 'bail|required|regex:/^[9][9][6][0-9]{9}$/',
            'note' => 'bail|string|max:1000',
            'address' => 'bail|string|max:250',
            'address_lat' => 'bail|numeric',
            'address_lng' => 'bail|numeric',
        ]);

        $phone_number = $this->request->get('phone_number');
        $category_id = $this->request->get('category_id');
        $note = $this->request->get('note');
        $address = $this->request->get('address');
        $address_lng = $this->request->get('address_lng');
        $address_lat = $this->request->get('address_lat');

        if (!Category::where('id', $category_id)->exists()) return [null, 'category_not_found'];

        if (
            (!$this->request->has('address') || strlen($address) === 0) &&
            (!$this->request->has('address_lat') ||
                !$this->request->has('address_lng') ||
                strlen($address_lat) === 0 ||
                strlen($address_lng) === 0)
        ) return [null, 'invalid_address'];

        $item = new Order();
        $item->publisher_id = $this->user->id;
        $item->category_id = $category_id;
        $item->phone_number = $phone_number;
        $item->note = $note;
        $item->address = $address;
        if ($address_lat) $item->address_lat = $address_lat;
        if ($address_lng) $item->address_lng = $address_lng;

        if (!$item->save()) throw new ResponseException();
        $item = Order::find($item->id);

        event(new OnCreateOrderDriverEvent($item));
        event(new OnCreateOrderAdminEvent($item));

        return [$item];
    }

    public function get()
    {
        /*
         * Response:
         *      item_not_found
         */

        $this->_validate([
            'id' => 'bail|integer',
        ]);

        if ($this->request->has('id')) {
            $item = Order::find($this->request->get('id'));
            if (!$item) return [null, 'item_not_found'];
            return [$item];
        } else {
            return [Order::all()];
        }
    }

    public function postCancel()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_cancelled
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];
        if (in_array($item->status, [
            'cancelled_by_admin', 'cancelled_by_client',
        ])) return [null, 'already_cancelled'];

        $history = [
            'changed_from' => $item->status,
            'changed_to' => 'cancelled_by_admin',
            'changed_by' => $this->user->type . '_' . $this->user->id,
            'changed_at' => Carbon::now(),
        ];

        $item->global_history = array_merge([$history], $item->global_history ? $item->global_history : []);
        $item->history = null;
        $item->status = 'cancelled_by_admin';
        $item->executor_id = null;

        if (!$item->save()) throw new ResponseException();

        event(new OnUpdateOrderDriverEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [$item];
    }
}
