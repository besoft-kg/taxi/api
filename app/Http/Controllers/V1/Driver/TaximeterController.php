<?php

namespace App\Http\Controllers\V1\Driver;

use App\Exceptions\ResponseException;
use App\Http\Controllers\V1\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\Taximeter;
use Carbon\Carbon;

class TaximeterController extends Controller
{
    public function postLocation()
    {
        /*
         * Response:
         *      -item_not_found
         */

        $this->_validate([
            'id' => 'bail|required|integer',
            'latitude' => 'bail|required|numeric',
            'longitude' => 'bail|required|numeric',
        ]);

        $item = Taximeter::where('id', $this->request->get('id'))->whereNull('ended_at')->where('driver_id', $this->user->id)->first();

        if (!$item) return [null, 'item_not_found'];

        $item->locations = array_merge($item->locations ? $item->locations : [], [
            [
                'lat' => $this->request->get('latitude'),
                'lng' => $this->request->get('longitude'),
            ]
        ]);

        if (!$item->save()) throw new ResponseException();

        return [$item];
    }

    public function postComplete()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_completed
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Taximeter::whereNull('order_id')
            ->where('id', $this->request->get('id'))
            ->where('driver_id', $this->user->id)
            ->first();

        if (!$item) return [null, 'item_not_found'];
        if ($item->ended_at) return [null, 'already_completed'];

        $item->ended_at = Carbon::now();

        if (!$item->save()) throw new ResponseException();

        return [$item];
    }

    public function postIndex()
    {
        /*
         * Response:
         *      -balance_is_not_enough
         *      -driver_is_busy
         *      -driver_has_active_order
         *      -driver_has_active_taximeter
         */

        $this->_validate([
            'latitude' => 'bail|required|numeric',
            'longitude' => 'bail|required|numeric',
        ]);

        if ($this->user->busy) return [null, 'driver_is_busy'];

        $item = Taximeter::where('id', $this->request->get('id'))->whereNull('ended_at')->where('driver_id', $this->user->id)->first();

        if (Order::active()
            ->where('executor_id', $this->user->id)
            ->count() > 0
        ) return [null, 'driver_has_active_order'];

        if ($item) return [null, 'driver_has_active_taximeter'];

        $category = Category::find($this->user->categories[0]);

        $item = new Taximeter();
        $item->driver_id = $this->user->id;
        $item->amount_to_pay = $category->landing_price;
        $item->amount_info = [
            'landing_price' => $category->landing_price,
            'price_for_km' => $category->price_for_km,
            'price_for_waiting_minute' => $category->price_for_waiting_minute,
            'price_for_duration_minute' => $category->price_for_duration_minute,
        ];

        $item->locations = [
            [
                'lat' => $this->request->get('latitude'),
                'lng' => $this->request->get('longitude'),
            ]
        ];

        if (!$item->save()) throw new ResponseException();

        return [$item];
    }
}
