<?php

namespace App\Http\Controllers\V1\Driver;

use App\Exceptions\ResponseException;
use App\Models\Category;
use App\Models\ConfirmPhone;
use App\Models\Driver;
use App\Models\Session;
use App\Services\SmsService;
use App\Utils\Generator;
use Carbon\Carbon;
use App\Http\Controllers\V1\Controller;

class AuthController extends Controller
{
    public function postIndex()
    {
        /*
         * Response:
         *      exists_code_was_sent
         *      exists_code_is_sent
         *      not_exists_code_was_sent
         *      not_exists_code_is_sent
         */

        $this->_validate([
            'phone_number' => 'bail|required|regex:/^[9][9][6][0-9]{9}$/'
        ]);

        $phone_number = $this->request->get('phone_number');

        $user = Driver::where('phone_number', $phone_number)->first();

        $code = $phone_number === '996504290100' ? 9345 : Generator::verificationCode();

        if ($user) {
            if (ConfirmPhone::where('user_id', $user->id)->where('user_type', 'driver')->where('confirmed', false)->where('attempts', '>', 0)->first()) {
                return [null, 'exists_code_was_sent'];
            } else {
                return $this->_dbTransactionAndTryCatch(function () use ($user, $code) {
                    $sms = new SmsService(
                        $user->phone_number,
                        'Код подтверждения вашего номера: ' . $code,
                        'sign-in',
                        ['code' => $code],
                        $user->phone_number === '996504290100'
                    );

                    $sms = $sms->send();

                    $confirm_phone = new ConfirmPhone();
                    $confirm_phone->sms_id = $sms->id;
                    $confirm_phone->user_id = $user->id;
                    $confirm_phone->user_type = 'driver';
                    $confirm_phone->phone_number = $user->phone_number;
                    $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

                    $confirm_phone->save();

                    return [null, 'exists_code_is_sent'];
                });
            }
        } else {
            if (ConfirmPhone::where('phone_number', $phone_number)->where('user_type', 'driver')->where('confirmed', false)->where('attempts', '>', 0)->first()) {
                return [null, 'not_exists_code_was_sent'];
            } else {
                return $this->_dbTransactionAndTryCatch(function () use ($phone_number, $code) {
                    $sms = new SmsService(
                        $phone_number,
                        'Код подтверждения вашего номера: ' . $code,
                        'sign-up',
                        ['code' => $code],
                        $phone_number === '996504290100'
                    );

                    $sms = $sms->send();

                    $confirm_phone = new ConfirmPhone();
                    $confirm_phone->sms_id = $sms->id;
                    $confirm_phone->user_type = 'driver';
                    $confirm_phone->phone_number = $phone_number;
                    $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

                    $confirm_phone->save();

                    return [null, 'not_exists_code_is_sent'];
                });
            }
        }
    }

    public function postLogin()
    {
        /*
         * Response:
         *      -code_is_incorrect
         *      -code_is_invalid
         *      -driver_not_found
         */

        $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'verification_code' => 'bail|required|integer|digits:4',
            'fcm_token' => 'bail|string',
            'platform' => 'bail|required|string|in:web,android,ios',
            'version_code' => 'bail|required|integer',
        ]);

        $phone_number = $this->request->get('phone_number');
        $verification_code = $this->request->get('verification_code');
        $fcm_token = $this->request->get('fcm_token');
        $platform = $this->request->get('platform');
        $version_code = $this->request->get('version_code');

        $user = Driver::where('phone_number', $phone_number)->first();

        if (!$user) return [null, 'driver_not_found'];

        $confirm_phone = ConfirmPhone::where('user_id', $user->id)->where('user_type', 'driver')->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) return [null, 'code_is_invalid'];

        return $this->_dbTransactionAndTryCatch(function () use (
            $confirm_phone,
            $verification_code,
            $user,
            $version_code,
            $platform,
            $fcm_token
        ) {
            if ($confirm_phone->encrypted_code === md5($verification_code . env('APP_KEY'))) {
                $confirm_phone->confirmed = true;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $sessionKey = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->user_type = 'driver';
                $session->key = $sessionKey;
                $session->expired_at = $expired_at;
                $session->version_code = $version_code;
                $session->platform = $platform;
                $session->user_agent = $this->request->header('User-Agent');

                if ($this->request->has('fcm_token') && $fcm_token) {
                    $session->fcm_token = $fcm_token;
                    Session::where('user_id', '!=', $user->id)->where('user_type', 'driver')->where('fcm_token', $fcm_token)->delete();
                }

                $session->save();

                if ($user->isDirty()) $user->save();

                return [[
                    'token' => Generator::jwt($user, $sessionKey, $expired_at->timestamp),
                    'user' => Driver::where('id', $user->id)->with([
                        'picture',
                        'vehicle_picture',
                        'datasheet_picture',
                        'drivers_license_picture',
                    ])->first(),
                    'categories' => Category::all(),
                ]];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                return [['attempts' => $confirm_phone->attempts], 'code_is_incorrect'];
            }
        });
    }

    public function postRegister()
    {
        /*
         * Response:
         *      -driver_already_exists
         *      -code_is_invalid
         *      -code_is_incorrect
         */

        $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'verification_code' => 'bail|required|integer|digits:4',
            'fcm_token' => 'bail|string',
            'platform' => 'bail|required|string|in:web,android,ios',
            'version_code' => 'bail|required|integer',
        ]);

        $phone_number = $this->request->get('phone_number');
        $verification_code = $this->request->get('verification_code');
        $fcm_token = $this->request->get('fcm_token');
        $platform = $this->request->get('platform');
        $version_code = $this->request->get('version_code');

        $user = Driver::where('phone_number', $phone_number)->first();

        if ($user) throw new ResponseException('', 'driver_already_exists');

        //if (!$confirm_phone) throw new ResponseException('Код недействительный или у вас нет попыток!', 'code_is_invalid');
        $confirm_phone = ConfirmPhone::where('phone_number', $phone_number)->where('user_type', 'driver')->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) return [null, 'code_is_invalid'];

        return $this->_dbTransactionAndTryCatch(function () use (
            $confirm_phone,
            $platform,
            $version_code,
            $verification_code,
            $phone_number,
            $fcm_token
        ) {
            if ($confirm_phone->encrypted_code === md5($verification_code . env('APP_KEY'))) {
                $user = new Driver();
                $user->phone_number = $phone_number;
                $user->balance = 0;
                $user->save();

                $confirm_phone->confirmed = true;
                $confirm_phone->user_id = $user->id;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $session_key = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->user_type = 'driver';
                $session->key = $session_key;
                $session->expired_at = $expired_at;
                $session->platform = $platform;
                $session->version_code = $version_code;
                $session->user_agent = $this->request->header('User-Agent');

                if ($this->request->has('fcm_token') && $fcm_token) {
                    $session->fcm_token = $fcm_token;
                    Session::where('user_id', '!=', $user->id)->where('user_type', 'driver')->where('fcm_token', $fcm_token)->delete();
                }

                $session->save();

                return [[
                    'token' => Generator::jwt($user, $session_key, $expired_at->timestamp),
                    'user' => Driver::where('id', $user->id)->with([
                        'picture',
                        'vehicle_picture',
                        'datasheet_picture',
                        'drivers_license_picture',
                    ])->first(),
                    'categories' => Category::all(),
                ]];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                throw new ResponseException(['attempts' => $confirm_phone->attempts], 'code_is_incorrect');
            }
        });
    }
}
