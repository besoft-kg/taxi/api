<?php

namespace App\Http\Controllers\V1\Driver;

use App\Models\Category;
use App\Http\Controllers\V1\Controller;

class CategoryController extends Controller
{
    public function get()
    {
        /*
         * Response:
         *      item_not_found
         */

        $this->_validate([
            'id' => 'bail|integer',
        ]);

        if ($this->request->has('id')) {

            $item = Category::where('id', $this->request->get('id'));
            if (!$item) return [null, 'item_not_found'];
            return [$item];

        } else {

            return [Category::all()];

        }
    }
}
