<?php

namespace App\Http\Controllers\V1\Driver;

use App\Events\Admin\OnUpdateOrderEvent as OnUpdateOrderAdminEvent;
use App\Events\Driver\OnUpdateOrderEvent as OnUpdateOrderDriverEvent;
use App\Events\Client\OnUpdateOrderEvent as OnUpdateOrderClientEvent;
use App\Exceptions\ResponseException;
use App\Models\Order;
use App\Http\Controllers\V1\Controller;
use App\Models\Taximeter;
use Carbon\Carbon;

class OrderController extends Controller
{
    public function get()
    {
        /*
         * Response:
         *      item_not_found
         */

        $this->_validate([
            'id' => 'bail|integer',
        ]);

        if ($this->request->has('id')) {

            $item = Order::find($this->request->get('id'));
            if (!$item) return [null, 'item_not_found'];
            return [$item];
        } else {

            return [
                [
                    'items' => Order::with('category')
                        ->where(function ($query) {
                            $query->whereNull('executor_id')
                                ->orWhere('executor_id', $this->user->id);
                        })
                        ->where('status', 'pending')
                        ->limit(30)
                        ->orderBy('created_at', 'desc')
                        ->get(),
                    'active' => Order::where('executor_id', $this->user->id)
                        ->active()
                        ->with('taximeter')
                        ->first(),
                ]
            ];
        }
    }

    public function postAccept()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_busy
         *      -driver_is_busy
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        if (
            Order::active()->where('executor_id', $this->user->id)->count() > 0
        ) return [null, 'driver_is_busy'];

        $item = Order::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];
        if ($item->status !== 'pending') return [null, 'already_busy'];

        $item->status = 'going_to_client';
        $item->executor_id = $this->user->id;

        $item->history = [
            [
                'changed_from' => 'pending',
                'changed_to' => 'going_to_client',
                'changed_by' => $this->user->type . '_' . $this->user->id,
                'changed_at' => Carbon::now(),
            ]
        ];

        $item->global_history = array_merge([
            [
                'changed_from' => 'pending',
                'changed_to' => 'going_to_client',
                'changed_by' => $this->user->type . '_' . $this->user->id,
                'changed_at' => Carbon::now(),
            ]
        ], $item->global_history ? $item->global_history : []);

        if (!$item->save()) throw new ResponseException();

        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));
        event(new OnUpdateOrderDriverEvent($item));

        return [$item];
    }

    public function postCancel()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_cancelled
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::find($this->request->get('id'));

        if (!$item) return [null, 'item_not_found'];
        if (in_array($item->status, [
            'cancelled_by_admin', 'cancelled_by_client',
        ])) return [null, 'already_cancelled'];
        if ($item->status === 'pending') return [null, 'cant_be_changed'];

        $item->global_history = array_merge($item->global_history ? $item->global_history : [], [
            [
                'changed_from' => $item->status,
                'changed_to' => 'pending',
                'changed_by' => $this->user->type . '_' . $this->user->id,
                'changed_at' => Carbon::now(),
            ]
        ]);
        $item->status = 'pending';
        $item->history = null;
        $item->executor_id = null;

        if (!$item->save()) throw new ResponseException();

        event(new OnUpdateOrderDriverEvent($item));
        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [$item];
    }

    public function postInPlace()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_in_place
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::where('executor_id', $this->user->id)
            ->where('id', $this->request->get('id'))->first();

        if (!$item) return [null, 'item_not_found'];
        if ($item->status === 'in_place') return [null, 'already_in_place'];
        if ($item->status !== 'going_to_client') return [null, 'cant_be_changed'];

        $history = [
            'changed_from' => $item->status,
            'changed_to' => 'in_place',
            'changed_by' => $this->user->type . '_' . $this->user->id,
            'changed_at' => Carbon::now(),
        ];

        $item->global_history = array_merge($item->global_history ? $item->global_history : [], [$history]);
        $item->history = array_merge($item->history ? $item->history : [], [$history]);
        $item->status = 'in_place';

        if (!$item->save()) throw new ResponseException();

        event(new OnUpdateOrderDriverEvent($item));
        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [$item];
    }

    public function postFakeCall()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_fake_call
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::where('executor_id', $this->user->id)
            ->where('id', $this->request->get('id'))->first();

        if (!$item) return [null, 'item_not_found'];
        if (in_array($item->status, ['fake_call_checking', 'fake_call'])) return [null, 'already_fake_call'];
        if ($item->status !== 'in_place') return [null, 'cant_be_changed'];

        $history = [
            'changed_from' => $item->status,
            'changed_to' => 'fake_call_checking',
            'changed_by' => $this->user->type . '_' . $this->user->id,
            'changed_at' => Carbon::now(),
        ];

        $item->global_history = array_merge($item->global_history ? $item->global_history : [], [$history]);
        $item->history = array_merge($item->history ? $item->history : [], [$history]);
        $item->status = 'fake_call_checking';

        if (!$item->save()) throw new ResponseException();

        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [$item];
    }

    public function postGoingWithClient()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_going_with_client
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::where('executor_id', $this->user->id)
            ->where('id', $this->request->get('id'))->first();

        if (!$item) return [null, 'item_not_found'];
        if ($item->status === 'going_with_client') return [null, 'already_going_with_client'];
        if ($item->status !== 'in_place') return [null, 'cant_be_changed'];

        $history = [
            'changed_from' => $item->status,
            'changed_to' => 'going_with_client',
            'changed_by' => $this->user->type . '_' . $this->user->id,
            'changed_at' => Carbon::now(),
        ];

        $item->global_history = array_merge($item->global_history ? $item->global_history : [], [$history]);
        $item->history = array_merge($item->history ? $item->history : [], [$history]);
        $item->status = 'going_with_client';

        $taximeter = new Taximeter();
        $taximeter->order_id = $item->id;
        $taximeter->driver_id = $this->user->id;
        $taximeter->amount_to_pay = $item->category->landing_price;
        $taximeter->amount_info = [
            'landing_price' => $item->category->landing_price,
            'price_for_km' => $item->category->price_for_km,
            'price_for_waiting_minute' => $item->category->price_for_waiting_minute,
            'price_for_duration_minute' => $item->category->price_for_duration_minute,
        ];
        $taximeter->locations = [
            //            [
            //                'lat' => $this->user->location_lat,
            //                'lng' => $this->user->location_lng,
            //            ]
        ];

        if (!$item->save() || !$taximeter->save()) throw new ResponseException();

        $taximeter->refresh();

        event(new OnUpdateOrderDriverEvent($item));
        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [[
            'order' => $item,
            'taximeter' => $taximeter,
        ]];
    }

    public function postComplete()
    {
        /*
         * Response:
         *      -item_not_found
         *      -already_completed
         *      -cant_be_changed
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Order::where('executor_id', $this->user->id)
            ->where('id', $this->request->get('id'))->first();

        if (!$item) return [null, 'item_not_found'];
        if ($item->status === 'completed') return [null, 'already_completed'];
        if ($item->status !== 'going_with_client') return [null, 'cant_be_changed'];

        $history = [
            'changed_from' => $item->status,
            'changed_to' => 'completed',
            'changed_by' => $this->user->type . '_' . $this->user->id,
            'changed_at' => Carbon::now(),
        ];

        $item->global_history = array_merge($item->global_history ? $item->global_history : [], [$history]);
        $item->history = array_merge($item->history ? $item->history : [], [$history]);
        $item->status = 'completed';

        $taximeter = Taximeter::where('order_id', $item->id)->first();
        $taximeter->ended_at = Carbon::now();

        if (!$item->save() || !$taximeter->save()) throw new ResponseException();

        event(new OnUpdateOrderAdminEvent($item));
        if ($item->client_id) event(new OnUpdateOrderClientEvent($item));

        return [[
            'order' => $item,
            'taximeter' => $taximeter,
        ]];
    }
}
