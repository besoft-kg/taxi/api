<?php

namespace App\Http\Controllers\V1\Driver;

use App\Events\Admin\OnUpdateDriverEvent;
use App\Events\Driver\OnCreateAlarmEvent as OnCreateAlarmDriverEvent;
use App\Events\Driver\OnDeleteAlarmEvent as OnDeleteAlarmDriverEvent;
use App\Events\Admin\OnCreateAlarmEvent as OnCreateAlarmAdminEvent;
use App\Exceptions\ResponseException;
use App\Models\Driver;
use App\Models\Image;
use App\Http\Controllers\V1\Controller;
use App\Models\Alarm;
use App\Models\Category;
use App\Models\Order;
use App\Models\Taximeter;
use Carbon\Carbon;

class UserController extends Controller
{
    public function postAlarm()
    {
        /*
         * Response:
         *      -limit_exceeded
         *      -already_created
         */

        $this->_validate([
            'address_lat' => 'bail|required|numeric',
            'address_lng' => 'bail|required|numeric',
        ]);

        if (Alarm::where('driver_id', $this->user->id)->where('created_at', '>', Carbon::today())->count() >= 3) return [null, 'limit_exceeded'];

        $item = new Alarm();
        $item->driver_id = $this->user->id;
        $item->address_lat = $this->request->get('address_lng');
        $item->address_lng = $this->request->get('address_lat');

        if (!$item->save()) throw new ResponseException();

        $item = Alarm::with('driver')->find($item->id);

        event(new OnCreateAlarmDriverEvent($item));
        event(new OnCreateAlarmAdminEvent($item));

        return [$item];
    }

    public function deleteAlarm()
    {
        /*
         * Response:
         *      -item_not_found
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $item = Alarm::where('id', $this->request->get('id'))->where('driver_id', $this->user->id)->first();

        if (!$item) return [null, 'item_not_found'];

        event(new OnDeleteAlarmDriverEvent($item));

        return [$item];
    }

    public function getBasic()
    {
        return [
            [
                'orders' => [
                    'pending' => Order::where(function ($query) {
                        $query->whereNull('executor_id')
                            ->orWhere('executor_id', $this->user->id);
                    })
                        ->where('status', 'pending')
                        ->limit(30)
                        ->orderBy('created_at', 'desc')
                        ->get(),
                    'active' => Order::with(['client'])
                        ->where('executor_id', $this->user->id)
                        ->active()
                        ->with('taximeter')
                        ->first(),
                ],

                'taximeter' => Taximeter::whereNull('ended_at')
                    ->whereNull('order_id')
                    ->where('driver_id', $this->user->id)
                    ->first(),

                'user' => Driver::where('id', $this->user->id)->with(['picture', 'vehicle_picture', 'datasheet_picture', 'drivers_license_picture'])->first(),

                'categories' => Category::all(),
            ]
        ];
    }

    public function postDocument()
    {
        /*
         * Response:
         *      -already_confirmed
         */

        $this->_validate([
            'type' => 'bail|required|string|in:picture,vehicle_picture,datasheet_picture,drivers_license_picture',
            'image' => 'bail|required|image',
        ]);

        if ($this->user->confirmed_by_id && $this->user->confirmed_at) return [null, 'already_confirmed'];

        $type = $this->request->get('type') . '_id';

        $image = new Image();
        $image->attachFile($this->request->file('image'), 'image');
        $image->save();

        $old_picture = $this->user[$type];

        $this->user->fill([$type => $image->id]);

        if ($this->user->save()) {

            if ($old_picture) Image::destroy($old_picture);
            $item = Driver::where('id', $this->user->id)->with([
                'picture',
                'vehicle_picture',
                'datasheet_picture',
                'drivers_license_picture',
            ])->first();

            event(new OnUpdateDriverEvent($item));

            return [$item];
        } else throw new ResponseException();
    }
}
