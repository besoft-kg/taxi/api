<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
use Exception;

class Controller extends BaseController
{
    protected $user;
    protected $request;

    public function __construct(Request $request)
    {
        $this->user = Auth::user();
        $this->request = $request;
    }

    public function _dbTransactionAndTryCatch($f)
    {
        $response = null;
        DB::beginTransaction();
        $throw = null;

        try {
            $response = $f();
        } catch (Exception $e) {
            if (!($e instanceof ResponseException)) DB::rollback();
            $throw = $e;
            $response = $e;
        }

        if (!$throw || $throw instanceof ResponseException) {
            DB::commit();
        }

        if ($throw) throw $throw;
        else return $response;
    }

    public function _checkWith($name)
    {
        return in_array($name, explode(',', $this->request->get('_with')));
    }

    public function _getRequestParams($keys = null, $additional = null)
    {
        $args = $this->request->all($keys);

        switch ($additional) {
            case 'get':
                if (empty($args['id'])) $args['id'] = null;
                if (empty($args['_limit']) || $args['_limit'] > 30) $args['_limit'] = 30;
                if (empty($args['_sorting'])) $args['_sorting'] = 'desc';
                if (empty($args['_order_by'])) $args['_order_by'] = 'id';
                if (empty($args['_after_by'])) $args['_after_by'] = null;
                if (empty($args['_after'])) $args['_after'] = null;
                if (empty($args['_with'])) $args['_with'] = null;
                break;
            default:
                break;
        }

        return $args;
    }

    public function _validate($rules, $for = null, $payload = null)
    {
        $validators = [];

        switch ($for) {
            case 'get':
                $validators = [
                    'id' => 'bail|integer',
                    '_order_by' => 'bail|string|in:id,created_at,updated_at' . ($payload ? ',' . $payload : ''),
                    '_sorting' => 'bail|string|in:desc,asc',
                    '_limit' => 'bail|integer|min:1|max:30',
                    '_after_by' => 'bail|string|in:id,created_at,updated_at' . ($payload ? ',' . $payload : ''),
                    '_after' => 'bail',
                    '_with' => 'bail|string',
                ];
                break;
            default:
                break;
        }

        $this->validate($this->request, array_merge(
            $validators,
            $rules
        ));
    }
}
