<?php

namespace App\Http\Controllers;

use App\Http\Controllers\V1\Controller;
use Illuminate\Support\Facades\Broadcast;

class BroadcastController extends Controller {

    public function authenticate()
    {
        return Broadcast::auth($this->request);
    }
}
