<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'auth'], function () use ($router) {
	$router->post('broadcasting/auth', 'BroadcastController@authenticate');
});

$router->group(['prefix' => 'api'], function () use ($router) {
	$router->group([
		'namespace' => 'V1',
		'prefix' => 'v1',
		'middleware' => 'app',
	], function () use ($router) {

		$router->get('/version/last', 'VersionController@get_last');

		// ADMIN

		$router->group([
			'namespace' => 'Admin',
			'prefix' => 'admin'
		], function () use ($router) {

			$router->group(['middleware' => ['admin']], function () use ($router) {

				// AUTH REQUIRED

				$router->get('/auth/me', 'AuthController@getUser');

				$router->post('/order', 'OrderController@postIndex');
				$router->get('/order', 'OrderController@get');

				$router->post('/order/cancel', 'OrderController@postCancel');

				//$router->post('/category', 'CategoryController@create');
				$router->get('/category', 'CategoryController@get');
				//$router->delete('/category', 'CategoryController@delete');
				//$router->put('/category', 'CategoryController@update');

				$router->put('/me', 'UserController@update_me');

				//$router->get('/me/session', 'UserController@get_session');
				$router->put('/me/session', 'UserController@update_session');

				//$router->post('/product', 'ProductController@create');
				$router->get('/driver', 'DriverController@get');
				//$router->delete('/product', 'ProductController@delete');
				//$router->put('/product', 'ProductController@update');

				$router->post('/driver/confirm', 'DriverController@postConfirm');
			});

			$router->post('/auth/phone', 'AuthController@checkPhone');
			$router->post('/auth/phone/login', 'AuthController@loginPhone');
		});

		$router->group([
			'namespace' => 'Driver',
			'prefix' => 'driver'
		], function () use ($router) {

			$router->group(['middleware' => ['driver']], function () use ($router) {

				// AUTH REQUIRED

				$router->post('/user/document', 'UserController@postDocument');

				$router->get('/user/basic', 'UserController@getBasic');

				$router->post('/user/alarm', 'UserController@postAlarm');
				$router->delete('/user/alarm', 'UserController@deleteAlarm');

				$router->post('/taximeter', 'TaximeterController@postIndex');

				$router->post('/taximeter/location', 'TaximeterController@postLocation');

				$router->post('/taximeter/complete', 'TaximeterController@postComplete');

				//$router->post('/taximeter/locations', 'TaximeterController@postLocations');

				$router->get('/order', 'OrderController@get');

				$router->post('/order/accept', 'OrderController@postAccept');

				$router->post('/order/cancel', 'OrderController@postCancel');

				$router->post('/order/in_place', 'OrderController@postInPlace');

				$router->post('/order/fake_call', 'OrderController@postFakeCall');

				$router->post('/order/going_with_client', 'OrderController@postGoingWithClient');

				$router->post('/order/complete', 'OrderController@postComplete');
			});

			$router->post('/auth/phone', 'AuthController@postIndex');
			$router->post('/auth/phone/login', 'AuthController@postLogin');
			$router->post('/auth/phone/register', 'AuthController@postRegister');
		});

		$router->group([
			'namespace' => 'Client',
			'prefix' => 'client'
		], function () use ($router) {

			$router->group(['middleware' => ['client']], function () use ($router) {

				// AUTH REQUIRED

				$router->get('/user/basic', 'UserController@getBasic');

				$router->post('/user/geocode', 'UserController@postGeocode');

				$router->post('/order', 'OrderController@postIndex');

				$router->post('/order/cancel', 'OrderController@postCancel');
			});

			$router->post('/auth/phone', 'AuthController@postIndex');
			$router->post('/auth/phone/login', 'AuthController@postLogin');
			$router->post('/auth/phone/register', 'AuthController@postRegister');
		});

		$router->get('/', function () {
			return response(['Besoft Taxi API is running...']);
		});
	});
});
