<?php

use App\Models\Order;
use Illuminate\Support\Facades\Broadcast;

// Private channels

Broadcast::channel('driver.orders', function ($user) {
    return $user->is_driver && !$user->busy;
});

Broadcast::channel('driver.alarm', function ($user) {
    return $user->is_driver && !$user->busy;
});

Broadcast::channel('admin.orders', function ($user) {
    return $user->is_admin;
});

Broadcast::channel('admin.drivers', function ($user) {
    return $user->is_admin;
});

Broadcast::channel('driver.{id}', function ($user, $id) {
    return $user->is_driver && $user->id === intval($id);
});

Broadcast::channel('client.{client_id}.order.{order_id}', function ($user, $client_id, $order_id) {
    $order = Order::find($order_id);
    return ($order &&
        $user->is_client &&
        $user->id === intval($client_id) &&
        $user->id === $order->client_id);
});
